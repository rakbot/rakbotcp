﻿using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    public class BotController : Controller
    {
        public IActionResult Auth(string serverIp, string name)
        {
            return Ok(name);
        }

        public IActionResult GetJob(string serverIp, string name)
        {
            return Ok(name);
        }

        public IActionResult SetJobStatus(string serverIp, string name, string status)
        {
            return Ok(name);
        }

        public IActionResult Disconnect(string serverIp, string name, string reason)
        {
            return Ok(name);
        }

        public IActionResult GetTask(string serverIp, string name)
        {
            return Ok(name);
        }
    }
}